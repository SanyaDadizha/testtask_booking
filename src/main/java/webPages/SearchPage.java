package webPages;

import base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;


public class SearchPage extends BasePage
{
    @FindBy (xpath="//div[@data-mode='checkin']/div/div[@class='sb-date-field__display']")
    public WebElement checkIn;

    @FindBy(xpath = "//div[@data-mode='checkout']/div/div[@class='sb-date-field__display']")
    public WebElement checkOut;

    @FindBy(xpath = "//input[@id='ss']")
    public WebElement citySearch;


    String expectedCheckInDate = getProperty("CheckInDate");
    String expectedCheckOutDate = getProperty("CheckOutDate");

    public void checkResultData() {

        implicitlyWait();
        mouseMoveToElementActions(checkIn); //It's need for tests on FireFox
        Assert.assertEquals(checkIn.getText(),expectedCheckInDate );

        mouseMoveToElementActions(checkOut);
        Assert.assertEquals(checkOut.getText(),expectedCheckOutDate );
    }
    public void checkCity(String city){
        Assert.assertEquals(citySearch.getAttribute("value"), city);
    }




}
