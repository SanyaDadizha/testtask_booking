package webPages;

import base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;


public class HomePage extends BasePage {


    @FindBy(xpath = "//input[@name='ss']")
    public WebElement cityField;

    @FindBy(xpath = "//span[contains(.,'→')]")
    public WebElement nextMouth;

    @FindBy(xpath = "//span[contains(.,'←')]")
    public WebElement previousMouth;

    @FindBy(xpath = "(//span[@class='c2-day-inner'][contains(.,'1')])[27]")
    public WebElement september_1;

    @FindBy(xpath = "(//span[@class='c2-day-inner'][contains(.,'30')])[18]")             //(//span[@class='c2-day-inner'][contains(.,'30')])[18]
    public WebElement september_30;

    @FindBy(xpath = "//div[@data-mode='checkout']")
    public WebElement check_Out;

    @FindBy(xpath = "//button[@class='sb-searchbox__button  ']")
    public WebElement buttonSearch;

    @FindBy(xpath = "//div[@class='fe_banner__message '] ")
    public WebElement errorMessage;

    @FindBy(xpath = "//li[@data-component-show-once-key='header_signin_prompt']//div/span")
    public WebElement signInButton;

    @FindBy(xpath = "//*[@class='user_access_email bootstrapped-input input-text input-block-level input-xlarge']")
    public WebElement email;

    @FindBy(xpath = "//*[@class='user_access_password bootstrapped-input input-text input-block-level input-xlarge']")
    public WebElement password;


    public static String getPageUrl() {
        return getProperty("base.url");
    }

    public void searchCity(String city) {
        cityField.clear();
        cityField.sendKeys(city);
    }

    public void selectCityFromDropDownList(String city) {
        WebElement cityElement = waitForPresenceOfElementLocated(By.xpath("(//b[contains(.,'"+city+"')])[1]"));
        mouseMoveToElementActions(cityElement);
        mouseClickActions(cityElement);
    }

    public void selectMouth(){
        WebElement selectElement = waitForElementToBeClickable(nextMouth);
        selectElement.click();
    }

    public void selectStartDay() {
        WebElement selectElement = waitForElementToBeClickable(september_1);
        mouseMoveToElementActions(selectElement);
        mouseClickActions(selectElement);
    }
    public void selectCheckOutDay() {
        //was changed code in calendar on booking.com

        //calendar does not work stably
        //sometimes this code from this place
        WebElement selectElement = waitForElementToBeClickable(check_Out);
        mouseMoveToElementActions(selectElement);
        selectElement.click();
        mouseClickActions(selectElement);
        //to this must to be commented

        WebElement selectElement2 = waitForElementToBeClickable(september_30);
        mouseMoveToElementActions(selectElement2);
        selectElement2.click();
    }

    public void pressSearchButton(){
        buttonSearch.click();
    }

    public void clearSearchField() {
        cityField.clear();
    }
    public void checkErrorMessage() {
        Assert.assertEquals(verifyElementIsPresent(errorMessage),true);
    }

    public void pressOnSignButton(){
        signInButton.click();
    }

    public void checkSignWindow(){
        verifyElementIsPresent(email);
        verifyElementIsPresent(password);
    }
}