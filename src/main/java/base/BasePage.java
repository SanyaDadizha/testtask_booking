package base;

import initial.BrowserFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.ConfigProperties;
import webPages.HomePage;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

public class BasePage {

    static private WebDriver driver;
    static private WebDriverWait wait;
    private static final Integer  TIMELOAD =10, IMPLICIT_WAIT = 10;

    {
        if(driver == null)
            driver = BrowserFactory.startBrowser();
        wait = new WebDriverWait(driver, TIMELOAD);
        PageFactory.initElements(driver, this);
        ConfigProperties.init();
    }

    public WebElement waitForPresenceOfElementLocated(By locator) {
        return wait.until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public WebElement waitForElementToBeClickable(WebElement element){
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void implicitlyWait(){
        driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT, TimeUnit.SECONDS);
    }

    public void mouseMoveToElementActions(WebElement element) {
        Actions mouseActions = new Actions(driver);
        mouseActions.moveToElement(element).build().perform();
    }

    public void mouseClickActions(WebElement element) {
        Actions mouseActions = new Actions(driver);
        mouseActions.click(element).build().perform();
    }

    public void loadPage() {
        driver.manage().window().maximize();
        driver.get(HomePage.getPageUrl());
    }
    public boolean verifyElementIsPresent(WebElement element) {
        try {
            element.getTagName();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static String getProperty(String key){
       return ConfigProperties.getProperties().getProperty(key);
    }
}
