package initial;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserFactory {

    static WebDriver driver;


    public enum BrowserType {
        FIREFOX_LINUX,
        CHROME_LINUX,
        FIREFOX_WIN,
        CHROME_WIN
    }
    // I use Ubuntu, for testing in Windows, please, use drivers from resources/driversWindows.

    public static WebDriver getDriver(BrowserType type) {

        switch (type) {
            case FIREFOX_LINUX:
                System.setProperty("webdriver.gecko.driver", "src/main/resources/driversLinux/geckodriver");
                return new FirefoxDriver();
            case CHROME_LINUX:
                System.setProperty("webdriver.chrome.driver", "src/main/resources/driversLinux/chromedriver");
                return new ChromeDriver();
            case FIREFOX_WIN:
                System.setProperty("webdriver.gecko.driver", "src/main/resources/driversWindows/geckodriver.exe");
                return new FirefoxDriver();
            case CHROME_WIN:
                System.setProperty("webdriver.chrome.driver", "src/main/resources/driversWindows/chromedriver.exe");
                return new ChromeDriver();
            default:
                return new FirefoxDriver();
        }
    }

    public static WebDriver startBrowser() {
        driver = getDriver(BrowserType.FIREFOX_LINUX);
        return driver;
    }
}
