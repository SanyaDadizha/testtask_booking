import org.testng.annotations.Test;
import webPages.HomePage;

public class CheckErrorMessage {

    HomePage homePage = new HomePage();

    @Test
    public void load() {
        homePage.loadPage();
    }

    @Test (dependsOnMethods = "load")
    public void checkErrorMessage(){
        homePage.clearSearchField();
        homePage.pressSearchButton();
        homePage.checkErrorMessage();
    }
}
