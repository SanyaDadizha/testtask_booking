import org.testng.annotations.Test;
import webPages.HomePage;
import webPages.SearchPage;


public class SearchHotels {

    HomePage homePage = new HomePage();
    SearchPage searchPage = new SearchPage();

    //Test works not only with city "New York"

    @Test
    public void load() {
        homePage.loadPage();
    }

    @Test (dependsOnMethods = "load")
    public void searchHotels(){
        homePage.searchCity(HomePage.getProperty("city"));
        homePage.selectCityFromDropDownList(HomePage.getProperty("city"));
        homePage.selectMouth();
        homePage.selectStartDay();
        homePage.selectCheckOutDay();
        homePage.pressSearchButton();
    }

    @Test (dependsOnMethods = "searchHotels")
    public void verification ()  {
        searchPage.checkResultData();
        searchPage.checkCity(HomePage.getProperty("city"));
    }
}
