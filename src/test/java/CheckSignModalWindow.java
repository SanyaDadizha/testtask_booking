import org.testng.annotations.Test;
import webPages.HomePage;


public class CheckSignModalWindow {

    HomePage homePage = new HomePage();

    @Test
    public void load() {
        homePage.loadPage();
    }

    @Test (dependsOnMethods = "load")
    public void checkSignButton() {
        homePage.pressOnSignButton();
        homePage.checkSignWindow();
    }
}